export default class Gamer {
  name;
  strenght;

  constructor(name, strenght) {
    this.name = name;
    this.strenght = strenght;
  }

  toString = () => {
    return (
      "The gamer: " + this.name + " has a level of strenght of:" + this.strenght
    );
  };

  isStrongherThan = (gamer) => {
    return this.strenght >= gamer.strenght;
  };
}
