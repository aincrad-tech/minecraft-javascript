export default class Match {
  player1;
  player2;
  winner;
  status = "Initialized";

  constructor(player1, player2) {
    this.player1 = player1;
    this.player2 = player2;
  }

  toString = () => {
    return "This match is in status: " + this.status;
  };

  start = () => {
    if (this.status === "Initialized") this.status = "Ongoing";
    else
      throw new Error(
        "Current match status is incompatible with start. The match is running in this moment"
      );
  };

  end = () => {
    this.winner = this.player1.isStrongherThan(this.player2)
      ? this.player1
      : this.player2;
    if (this.status === "Ongoing") {
      this.status = "Ended";
      console.log("The winner is " + this.winner.name);
    } else
      throw new Error(
        "Current match status is incompatible with end. The match still has to start"
      );
  };
}
