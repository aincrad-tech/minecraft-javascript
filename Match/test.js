import Gamer from "./Gamer.js";
import Match from "./Match.js";

let leo = new Gamer("Leo", 100);
let loris = new Gamer("Loris", 90);

let match = new Match(leo, loris);

match.start();
match.end();
