class world{

    constructor(){
        this.name;
        this.master;
        this.seed;
    }

    random = () =>{
        let num;
        num = Math.random()+100;
        return num;
    }

    generateSeed = () =>{
        const X = 4;
        let i;
        let generated = '';
        
        for(i=0; i<X; i++){
            if (i++ == X){
                generated = generated + this.random();
            }
            else {
                generated = generated + this.random() + '.';
            }
        }
    }

    nameToStirng = (NOME) => {
        this.name = NOME;
    }

    masterToString = (MASTER) => {
        this.master = MASTER;
    }
}